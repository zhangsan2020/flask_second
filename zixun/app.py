import logging

from flask_script import Manager
from flask_migrate import Migrate,MigrateCommand
from config import Config
from info import create_app,db
from info import models


app = create_app()

manager = Manager(app)
Migrate(app,db=db)
manager.add_command('db',MigrateCommand)
# @app.route('/setcookie')
# def set_cookie():
#     resp = make_response('this is to set cookie')
#     resp.set_cookie('username','张三')
#     resp.set_cookie('age','34')
#
#     # resp.set_cookie('username','张磊')
#
#     return resp
#
# @app.route('/getcookie')
# def get_cookie():
#     # abort(404)
#
#     resp = request.cookies
#     # resp.get_('username','itcast')
#     if resp['username'] == '张磊':
#         resp = '张先生'
#     else:
#         resp ='不能进入'
#     return resp
#
# @app.route('/set_session')
# def set_session():
#     session['username'] = 'qqqqqqqqqqq'
#     return 'set_session ok!'
#
# @app.route('/get_session')
# def get_session():
#     print(session.get('username'))
#     # abort(500)
#
#     return session.get('username')
#
#

@app.route('/')
def hello_world():
    logging.warning('有意思吗')
    return 'Hello World!'
#
#
# @app.before_first_request
# def before_first_request():
#     print("before_first_request")
#
# @app.before_request
# def before_request():
#     print("before_request")
#
# @app.errorhandler(404)
# def internal_server_error(e):
#     return '服务器搬家了'
#
#
# @app.after_request
# def after_request(response):
#     print("after_request")
#     response.headers["Content-Type"] = "application/jsondsafd"
#     # response.headers["sta"]
#     return response

if __name__ == '__main__':
    # app.run()
    # print(app.url_map)
    manager.run()