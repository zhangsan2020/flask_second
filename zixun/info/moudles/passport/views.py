import random
import re

from info.moudles.passport import passport_blue
from info.utils.captcha.captcha import captcha
from flask import request, jsonify, make_response, current_app
from info import constants
from info import redis_store
from info.response_code import RET

from flask_second.zixun.info.lib.yuntongxun.sms import CCP
from flask_second.zixun.info.models import User
from flask_second.zixun.info.response_code import error_map
# from info.lib.yuntongxun.CCPRestSDK import CCP


@passport_blue.route('/img_code')
def img_code():

    code_id = request.args.get('uuid')
    name, text, img = captcha.generate_captcha()
    try:
        redis_store.setex('img_'+code_id,constants.IMAGE_CODE_REDIS_EXPIRES,text)
    except Exception as e:
        return jsonify(errorno=RET.DATAERR,errormsg=e)
    response = make_response(img)
    response.headers['Content-Type'] = 'image/jpeg'
    return response
@passport_blue.route('/sendsms',methods=['POST'])
def sendSMSCode():

    json_args = request.json
    mobile = json_args['mobile']
    img_code = json_args['imageCode']
    img_code_id = json_args['imageCode_id']
    if not all([mobile,img_code,img_code_id]):
        return jsonify(errorno=RET.DATAERR,errormsg='请填写完整的参数')
    if not re.match('1[3-9][0-9]{9}',mobile):
        return jsonify(errorno=RET.DATAERR,errormsg='请填写正确的手机号码')
    count = User.query.filter(User.mobile==mobile).count()
    if count:
        return jsonify(errorno=RET.DATAEXIST,errormsg=error_map.RET.DATAEXIST)

    try:
        img_code_id_store = redis_store.get('img_'+img_code_id)
    except:
        return jsonify(errorno=RET.NODATA,errormsg="redis存储有问题")

    if not img_code_id_store:
        return jsonify(errorno=RET.DATAERR,errormsg='验证码已过期')


    if img_code_id_store.decode().lower() == img_code.lower():
        sms_code = '%06d'%random.randint(0,9999)
        ccp = CCP()
        ccp.send_template_sms('15210559392', [sms_code, 5], 1)
    else:
        return jsonify(errorno=RET.DATAERR,error_msg='验证码不一致!!')


    try:
        # setex(key,seconds,value)
        redis_store.setex('sms_'+mobile,constants.SMS_CODE_REDIS_EXPIRES,sms_code)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DATAERR,errmsg='发送失败')

    # 10. 返回相应
    return jsonify(errno=RET.OK,errmsg='ok')

@passport_blue.route('/register',methods=['post'])
def register():

    request_args = request.json
    mobile = request_args['mobile']
    password = request_args['password']
    sms_code = request_args['sms_code']
    if not all([mobile,password,sms_code]):
        return jsonify(errorno=RET.DATAERR,error_msg='请将注册数据填写完整')
    try:
        redis_sms_code = redis_store.get('sms_'+mobile)
    except Exception as e:
        current_app.logger(e)
        return jsonify(errorno=RET.DATAERR,errormsg='redis获取短信验证码失败')
        pass
    if not (redis_sms_code.decode().lower() == sms_code.lower()):
        return jsonify(errorno=RET.DATAERR,errnomsg='短信验证码不正确')

    user = User()
    user.mobile = mobile
    user.password = password
    return jsonify(errno='1')
