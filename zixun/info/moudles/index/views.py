from info.moudles.index import admin_home
from flask import render_template, current_app


@admin_home.route('/')
def index():
    return render_template('news/index.html')

@admin_home.route('/favicon.ico')
def favicon():
    return current_app.send_static_file('news/favicon.ico')