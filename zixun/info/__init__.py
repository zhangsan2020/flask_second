from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import redis
from flask_wtf.csrf import CSRFProtect
from flask_session import Session
from config import config


app = Flask(__name__)
db = SQLAlchemy()
redis_store = None
def create_app(config_name = 'develop'):

    # 配置对象，里面定义需要给 APP 添加的一系列配置
    global redis_store
    redis_store = redis.StrictRedis(host=config[config_name].REDIS_HOST,port=config[config_name].REDIS_PORT,db=1)
    # 从配置对象中加载配置
    app.config.from_object(config[config_name])
    # CSRFProtect(app)
    Session(app)
    db.init_app(app)
    from info.moudles.index import admin_home
    from info.moudles.passport import passport_blue
    app.register_blueprint(admin_home)
    app.register_blueprint(passport_blue)
    return app

