import redis
# print(__name__)
class Config(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql://root:123456@127.0.0.1:3306/zixun'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = '6379'
    # secret_key = 'ZHANGLEI'
    SECRET_KEY = 'ZHANGLEI'
    SESSION_TYPE = 'redis'
    SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST,port=REDIS_PORT,db=1)
    SESSION_USE_SIGNER = True
    # SESSION_PERMANENT = False
    PERMANENT_SESSION_LIFETIME = 3600

class DevelopmentConfig(Config):

    DEBUG = True

class ProductConfig(Config):

    DEBUG = False


config = {
    'develop':DevelopmentConfig,
    'product':ProductConfig
}